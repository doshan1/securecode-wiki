# Contributing Guidelines

Contributions are welcome via Gitlab Pull Requests. This document outlines the process to help get your contribution accepted.

Any type of contribution is welcome: new code snippets, secure code for a language which is not initially present, bug fixes, documentation improvements, etc.

## How to Contribute

Fork this repository, develop, and test your changes.
Submit a pull request.

### Contribution Details

Download the [template](./template.md), and make necessary changes and upload it in the `content/docs/lang/` directory.

Change the front matter, example below,

```yaml
---
title: "Title Here"
lead: "Subtitle Here"
description: "Small description about the content"
draft: false # Change it to true if you want to publish it
menu:
  docs:
    parent: "docs"
toc: true
author: "Name of the author(s) here"
---
```

The upload directory hierarchy is as follow,

```text
.
|- content
    |- docs
        |- lang
            |- your-file.md

```

## Requirements

### When submitting a PR make sure that:

* It must pass CI jobs for linting and test the changes (if any).
* The title of the PR is clear enough.
* If necessary, add information to the repository's README.md.
* PR Approval and Release Process
* Changes are manually reviewed by Payatu team members.
* The PR is merged by the reviewer(s) in the Gitlab's master branch.
* Then our CI/CD system is going to push the changes to the website including the recently merged changes.

## To add your contributor profile

Create a new directory inside `content > contributors`, with your name and inside that a file named `_index.md`, where all the details will be there.

Use the template [author-name/_index.md](./content/contributors/author-name/_index.md) , in the `contributors` directory.
